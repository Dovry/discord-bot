# Discord bot
Discord bot I'm working on to learn python 3

Requires
* 'token_file.py' to exist in the same folder as bot.py, with the contents:

```python
token = "your_token_here"
```

* python3.6 & pip

* discord.py - rewrite version


## how to install python3.6, pip and discord.py

first check if you have python 3.6 or later installed
```bash
python3 --version
```
it should return
```bash
Python 3.6.X
```
if it doesn't, run
```bash
sudo apt update
sudo apt install python3.6
```
Now that python3.6 is installed, install pip
```bash
sudo apt install python3-pip
```
then, with pip installed, run
```bash
python3.6 -m pip install -U https://github.com/Rapptz/discord.py/archive/rewrite.zip#egg=discord.py
```
to install discord.py

## to run the bot, do
```bash
python3.6 /path/to/bot.py
```
