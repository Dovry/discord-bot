import discord
from token_file import token

client = discord.Client()

@client.event # event decorator/wrapper
async def on_ready():
    print(f"logged in as {client.user}")

@client.event
async def on_message(message):
    print(f"{message.channel}: {message.author}: {message.author.name}: {message.content}:")
    
    if "hello" in message.content.lower():
        await message.channel.send("Hi :smile:")

    elif "bye" in message.content.lower():
    	await message.channel.send("See Ya! :wave:")
 
client.run(token)
